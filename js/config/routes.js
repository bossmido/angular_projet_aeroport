DEFER_LIST_MOCHE=[];
app_aeroport.config(['$routeProvider','$locationProvider',function($routeProvider,$locationProvider) {
    var chargerControlleur = function(path) {
        if(path=="MainAppController"){//contenrolleur de base donc déjà chargé
            return;
        }
var scr  = document.createElement('script'),
    head = document.head || document.getElementsByTagName('head')[0];
    scr.src = CONFIG.controller+path+".js";
    scr.async = false;
    head.insertBefore(scr, head.firstChild);
    
    return path;
        };
        $call_pass_param =  {
          properties: function() {
            return { dev : true };
        }};

        $routeProvider
        .when('/liste',
        {
            controller: chargerControlleur('listeController'),
            templateUrl: CONFIG.partial+"liste"+CONFIG.suffixe_template,
            resolve:$call_pass_param
        })
           .when('/choix_vol',
        {
            controller: chargerControlleur('choixVolController'),
            templateUrl: CONFIG.partial+'choix_vol'+CONFIG.suffixe_template,
            resolve:$call_pass_param
        })
        .when('/choix_vol/:passagerid',
        {
            controller: chargerControlleur('choixVolController'),
            templateUrl: CONFIG.partial+'choix_vol'+CONFIG.suffixe_template,
            resolve:$call_pass_param
        })
        .when('/edition_passager/:passagerid',
        {
            controller: chargerControlleur('editionPassagerController'),
            templateUrl: CONFIG.partial+'editionPassager'+CONFIG.suffixe_template,
            resolve:$call_pass_param
        }) .when('/edition_passager',
        {
            controller: chargerControlleur('editionPassagerController'),
            templateUrl: CONFIG.partial+'editionPassager'+CONFIG.suffixe_template,
            resolve:$call_pass_param
        })
        .when('/tout',
        {
            controller: chargerControlleur('MainAppController'),
            templateUrl: CONFIG.partial+'tout'+CONFIG.suffixe_template,
            resolve:$call_pass_param
        })
         .when('/tout:passagerid',
        {
            controller: chargerControlleur('MainAppController'),
            templateUrl: CONFIG.partial+'tout'+CONFIG.suffixe_template,
            resolve:$call_pass_param
        })
        .when('/validation',
        {
            controller: chargerControlleur('validationController'),
            templateUrl: CONFIG.partial+'validation'+CONFIG.suffixe_template,
            resolve:$call_pass_param
        }).when('/dependance_injection',
        {
            controller: chargerControlleur('dependancyInjectionController'),
            templateUrl: CONFIG.partial+'dependance_injection'+CONFIG.suffixe_template,
            resolve:$call_pass_param
        }).otherwise({ redirectTo: '' });
        $locationProvider.html5Mode(false);


    }]);

