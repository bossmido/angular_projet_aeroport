 var resolveController = function(path) {
            // var injector = angular.injector(['ng']),
            //var $cp = angular.injector().get('$controller');
            //console.log("%O",$cp);
            //  $q = injector.get('$q');
            // $scope = injector.get('$scope');

            // var deferred = $q.defer();
            //var script = document.createElement('script');
            var src = "/js/controllers/"+path+".js";
            /*script.onload = function() {
                $scope.$apply(deferred.resolve());
            };*/
            //document.head.appendChild(script);
            console.log(src);
            $.ajax({
                async:false,
                type:'GET',
                url:src,
                data:null,
                dataType:'script',
                success: function(data){
                    try {
                        window.eval(data);
                    }catch (e) {
                        alert(data);
                        alert("fichier malformé : "+src+" l."+e.getMessage());
                    }
                }
            });
            //console.log(src);
            //console.log('de la route : du controlleur : '+camelize(path)+"Controller")
            return camelize(path)+"Controller";
        };
        
        resolveController('choix_vol');
        resolveController('liste');
        resolveController('validation');
 
        myApp.config(['$routeProvider',function($routeProvider) {
           
          
            $routeProvider
             .when('/choix_vol',
            {
                controller: resolveController('choix_vol')        ,templateUrl: 'js/controllers/choix_vol.html'

            }).when('/choix_vol/:clientID',
            {
                controller: resolveController('choix_vol'),
                templateUrl: 'js/controllers/choix_vol.html'
        //,        resolve:$call_pass_param
            })
           .when('/liste',
            {
                controller: resolveController('liste'),
                templateUrl: "js/controllers/liste.html",
                resolve: ['$route', '$q',
                        function($route, $q) {
                            console.log("debut finit");
                            var routeDone = $q.defer();
                                routeDone.resolve(function(){
                                    $('.choix_date').datepicker({ dateFormat: 'yyy-dd-mm' });
                                });
                            return routeDone.promise;
                        }
                    ]
                //,resolve:$call_pass_param
            })

            .when('/validation',
            {
                controller: resolveController('validation'),
                templateUrl: 'js/controllers/validation.html'
                //,resolve:$call_pass_param
            }).otherwise({ redirectTo: '' });
        //    $locationProvider.html5Mode(true);
        /*
            when('/dependance_injection',
            {
                controller: resolveController('dependance_injection'),
                templateUrl: 'js/controllers/dependance_injection.html'
                //,resolve:$call_pass_param
            })
            */
        }]);
    
