//window.setTimeout = document.setTimeout ;
//voir config/init_config.js
myApp.controller("MainAppController",[
    '$scope',
    '$route',
    '$window',
    '$templateCache',
    '$http',
    '$compile',
    '$sce',
    '$templateRequest',
    'CONFIGService',
    'PassagerService',
    'dependanceService',
    function($scope,$route,$window,$templateCache,$http,$compile,$sce,$templateRequest,CONFIGService,PassagerService,dependanceService){
    var module = this;
    $rootScope = getRootScope();
    console.log("app lancé");


    var $http = angular.injector(["ng"]).get("$http");
    $p1 = charger_dependance($http,"js","data","users-100.js");
    $p2=charger_dependance($http,"js","data","donnee_extra.txt");

    ////////////////////////////////////////
    $rootScope.reset_view = function(evt){
        var currentPageTemplate = $route.current.templateUrl;
        $templateCache.remove(currentPageTemplate);
        $route.reload();
        $window.location.reload()
    }

    $scope.reset = function(evt){
        //$scope.telephone.$setPristine();
        //$scope.prenom.$setPristine();
    }

    /////////////////////////////////////////////
    
    $scope.CONFIG   = CONFIGService.CONFIG;
    $scope.alert    = CONFIG.partial+"alert.html";

   //////////////////////////////////////////////
    //ahha je compile encore
    var templateUrl = $sce.getTrustedResourceUrl('partial/init.html');

    $templateRequest(templateUrl).then(function(template) {
        $compile($("#init").html(template).contents())($scope);
    });
    
    var templateUrl = $sce.getTrustedResourceUrl('entete.html');

    $templateRequest(templateUrl).then(function(template) {
        $compile($("#entete").html(template).contents())($scope);
    });
        
    var templateUrl =$sce.getTrustedResourceUrl('contenu.html');

    $templateRequest(templateUrl).then(function(template) {
        $compile($("#contenu").html(template).contents())($scope);
    });
    
    var templateUrl = $sce.getTrustedResourceUrl('footer.html');

    $templateRequest(templateUrl).then(function(template) {
        $compile($("#footer").html(template).contents())($scope);
    });
    
    var templateUrl = $sce.getTrustedResourceUrl($scope.alert);

    $templateRequest(templateUrl).then(function(template) {
        $compile($("#alert").html(template).contents())($scope);
    });
    /////////////////////////////////////
    //scharger_style_app($scope,$http);
    randomiser_couleur($scope,$http);
    console.log("...complétement =] ");
    //alert($(window).height());
    $('body').css({"height": ($(window).height()-50)+"px"});
    $('.contenu').css({"height": ($('.contenu').parent().height())+"px"});



    $rootScope.alert_test = function(){
        if($rootScope.length >0){
            return true;
        }else{
            return false;
        }
    }

    $rootScope.$broadcast('finit_controlleur_principal');

}]);
    
        


////