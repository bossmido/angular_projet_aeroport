/* chargement boostrap depuis le JS parce que c'est lourd */
function charger_style_app($scope_ref,$http_ref) {
    var async=false;
    if(arguments.length>=3){
        if(arguments[2]==true){
            aysnc=true
        }
    }
    var inc_chargement = 0 ;
    var i =0;
    var controlleur_gestion_dependance_css = "index.html";
    var base_recherche = "js/vendor/bootstrap/css/dist/";
    var base_recherche_normal = "css/";
    var list_fichier =["bootstrap-reboot.css","bootstrap.css"];

    for(var i=0;list_fichier.length;i++){
        var self = this;
        var success = false;
        var res_requete  = $http_ref.get(base_recherche+list_fichier[i], 
        {
          params: 
          {
              target: base_recherche+list_fichier[i]
          }

      });

        res_requete.then(
            function(answer) {
                self.success = true;
            },
            function(error) {
                // report something
            },
            function(progress) {
            // report progress
        });


        var element_style_en_plus = document.createElement('link');
        element_style_en_plus.rel = 'stylesheet';
        if(success){
            element_style_en_plus.href = base_recherche+list_fichier[i];

        }else{
            element_style_en_plus.href = base_recherche_normal+list_fichier[i];

        }
        element_style_en_plus.type = 'text/css';
        var defer_de_fin = document.getElementsByTagName('link')[0];
        defer_de_fin.parentNode.insertBefore(element_style_en_plus, defer_de_fin);
    }
}

function charger_donnees_app($scope_ref,$http_ref) {
        //penser à utiliser le localdb, autre astuces de cache de navigateur
        var PCD =  "data/";
        liste_url = {"extra":"","users100":"","users20":""};
        liste_url['extra'] = PCD+"Donnee extra.txt";
        liste_url['users20'] = PCD+"users-20.js";
        liste_url['users100'] =  PCD+"users-100.js";
        for(  url  in liste_url ){
            var res_requete = $http_ref.get(liste_url[url]);
            
            var self = this;
            res_requete.then(
                function(answer) {
                    self.success = true;
                },
                function(error) {
                // report something
            },
            function(progress) {
            // report progress
        });


        }
    }



    function charger_dependance($http,$type,chemin_prefix,fichier_charger) {
        var async=false;
        if(arguments.length>=3){
            if(arguments[2]==true){
                aysnc=true
            }
        }


        var $q= angular.injector(["ng"]).get("$q");

        var inc_chargement = 0 ;
        var i =0;
        var base_recherche = chemin_prefix;
        var base_recherche_normal = "css/";
        var list_fichier =["bootstrap-reboot.css","bootstrap.css"];

        var controlleur_gestion_dependance_css = fichier_charger;

        if(typeof fichier_charger == "string"){
            var deferred = $q.defer();

            if(async==false){
                $.ajax({

                    async:false,

                    type:'GET',

                    url:(base_recherche+"/"+fichier_charger),

                    data:null,

                    success:function(answer){
                        window.eval(answer.data);

                    },
                    dataType:'script'
                });
            }else{
                var res_requete  = $http.get(base_recherche+"/"+fichier_charger, 
                {
                  params: 
                  {
                      target: base_recherche+fichier_charger
                  }
              });
                res_requete.then(
                    function(answer) {
                //console.log("%O",answer);
                window.eval(answer.data);
                });

            }

            var element_style_en_plus = null;
            if($type == "css"){
                var element_style_en_plus = document.createElement('link');
                element_style_en_plus.rel = 'stylesheet';
            }else if($type=="js"){
                var element_style_en_plus = document.createElement('script');
            }
            if(success){
                element_style_en_plus.href = base_recherche+fichier_charger;
                
            }else{
                element_style_en_plus.href = base_recherche_normal+fichier_charger;
                
            }

            var defer_de_fin = null;
            if($type == "css"){
                element_style_en_plus.type = 'text/css';
                defer_de_fin = document.getElementsByTagName('link')[0];
            }else if($type=='js'){
                element_style_en_plus.type = 'text/javascript';
                defer_de_fin = document.getElementsByTagName('script')[0];
                
            }            
            defer_de_fin.parentNode.insertBefore(element_style_en_plus, defer_de_fin);


            return deferred.promise;
        }else{
            controlleur_gestion_dependance_css = fichier_charger;
        }
        for(var i=0;controlleur_gestion_dependance_css.length;i++){
           console.log("%O",base_recherche+"/"+controlleur_gestion_dependance_css[i]);

           var self = this;
           var success = false;
           var res_requete  = $http.get(base_recherche+"/"+controlleur_gestion_dependance_css[i], 
           {
              params: 
              {
                  target: base_recherche+controlleur_gestion_dependance_css[i]
              }

          });
           res_requete.then(
            function(answer) {
                self.success = true;
            },
            function(error) {
                // report something
            },
            function(progress) {
            // report progress
        });

           var element_style_en_plus = null;
           if($type == "css"){
            var element_style_en_plus = document.createElement('link');
            element_style_en_plus.rel = 'stylesheet';
        }else if($type=="js"){
         var element_style_en_plus = document.createElement('script');
     }
     if(success){
        element_style_en_plus.href = base_recherche+list_fichier[i];

    }else{
        element_style_en_plus.href = base_recherche_normal+list_fichier[i];

    }
    var defer_de_fin = null;
    if($type == "css"){
        element_style_en_plus.type = 'text/css';
        defer_de_fin = document.getElementsByTagName('link')[0];
    }else if($type=='js'){
        element_style_en_plus.type = 'text/javascript';
        defer_de_fin = document.getElementsByTagName('script')[0];

    }            
    defer_de_fin.parentNode.insertBefore(element_style_en_plus, defer_de_fin);
}
}

function getRootScope(){
    var injector  =   angular.element(document.body).injector()
    var $rootScope = injector.get('$rootScope');
    return $rootScope;
}
