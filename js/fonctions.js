/* chargement boostrap depuis le JS parce que c'est lourd */
    function charger_style_app($scope_ref,$http_ref) {
    var inc_chargement = 0 ;
    var i =0;
        var controlleur_gestion_dependance_css = "index.html";
        var base_recherche = "js/vendor/bootstrap/css/dist/";
        var base_recherche_normal = "css/";
        var list_fichier =["bootstrap-reboot.css","bootstrap.css"];
        
        for(i=0;list_fichier.length;i++){
            var self = this;
            var success = false;
            var res_requete  = $http_ref.get(base_recherche+list_fichier[i], 
                      {
                          params: 
                          {
                              target: base_recherche+list_fichier[i]
                        }
                          
                    });
            
            res_requete.then(
            function(answer) {
                self.success = true;
            },
            function(error) {
                // report something
            },
            function(progress) {
            // report progress
            });
         
            
            var element_style_en_plus = document.createElement('link');
            element_style_en_plus.rel = 'stylesheet';
            if(success){
                element_style_en_plus.href = base_recherche+list_fichier[i];
                
            }else{
                element_style_en_plus.href = base_recherche_normal+list_fichier[i];
                
            }
            element_style_en_plus.type = 'text/css';
            var defer_de_fin = document.getElementsByTagName('link')[0];
            defer_de_fin.parentNode.insertBefore(element_style_en_plus, defer_de_fin);
        }
    }
        
      function charger_donnees_app($http_ref) {
        //console.log("%O",$http_ref)
        //penser à utiliser le localdb, autre astuces de cache de navigateur
        var PCD               =  "data/";
        liste_url             = {"extra":"","users100":"","users20":""};
        liste_url.extra    = PCD+"Donnee extra.js";
        ///liste_url['users20'] = PCD+"users-20.js";
        liste_url.users100 =  PCD+"users-100.js";
        for( var url  in liste_url ){
          /*  $req = {
                url : liste_url[url] , 
                method: 'GET',
                headers: {'Content-Type': 'text/javascript'} 
                
            }
            */
          
            console.log("%O",$http_ref.defaults.headers);
            var req =  liste_url[url];
            //alert(req.substr((req.length-3),req.length));

            var res_requete = $http_ref.get(req);
            
            var self = this;

            res_requete.then(
            function(answer) {
               // console.log("%O","function(){ "+answer.data+" }();");
                //document.context_eval = eval("(function(){ var test = function(){"+answer.data+"; }  ; return test; })();");
             //   console.log(answer.data);
             try {
                var suffix = req.substr((req.length-3),req.length);
                //alert(suffix);
                if( suffix != "html" || suffix != "" ){//on evite d'interpréter les templates mai en même temps des données éttait données en txt donc tout est du javascript par défaut
                    window.eval(answer.data);
                }

            } catch (e) {
                    if (e instanceof SyntaxError) {
                        if(req == ""){
                            //alert("fichier malformé contenu vu que aucucn fichier : "+answer.data);
                        }else{
                            alert("fichier malformé : "+req);
                        }
                    }
            }
                self.success = true;
            },
            function(error) {
                // report something
            },
            function(progress) {
                // report progress
            });
        }
      }
               
         function charger_dependance($scope_ref,$http_ref,$type,fichier_charger,chemin_prefix) {
    var inc_chargement = 0 ;
    var i = 0;
        var controlleur_gestion_dependance_css = fichier_charger;
        var base_recherche = chemin_prefix;
        var base_recherche_normal = "css/";
        var list_fichier =["bootstrap-reboot.css","bootstrap.css"];
        
        for(var i=0;list_fichier.length;i++){
            var self = this;
            var success = false;
            var res_requete  = $http_ref.get(base_recherche+list_fichier[i], 
                      {
                          params: 
                          {
                              target: base_recherche+list_fichier[i]
                        }
                          
                    });
            
            res_requete.then(
            function(answer) {
                self.success = true;
            },
            function(error) {
                // report something
            },
            function(progress) {
            // report progress
            });
            
            var element_style_en_plus = null;
            if($type == "css"){
                var element_style_en_plus = document.createElement('link');
                element_style_en_plus.rel = 'stylesheet';
            }else if($type == "js"){
                   var element_style_en_plus = document.createElement('script');
            }
            if(success){
                element_style_en_plus.href = base_recherche+list_fichier[i];
                
            }else{
                element_style_en_plus.href = base_recherche_normal+list_fichier[i];
                
            }
            var defer_de_fin = null;
            if($type == "css"){
                element_style_en_plus.type = 'text/css';
                defer_de_fin = document.getElementsByTagName('link')[0];
            }else if($type == 'js'){
                element_style_en_plus.type = 'text/javascript';
                defer_de_fin = document.getElementsByTagName('script')[0];
                
            }            
defer_de_fin.parentNode.insertBefore(element_style_en_plus, defer_de_fin);
        }
    }
    
   function compile_template($scope_ref,$sce_ref,$templateRequest_ref,$compile_ref,path,$output_tag) {
    var templateUrl =$sce_ref.getTrustedResourceUrl(path);
     $templateRequest_ref(templateUrl).then(function(template) {
        return $compile_ref($($output_tag).html(template).contents())($scope_ref);
    });
   }

   
   function camelize(input) {
    return input.toLowerCase().replace(/_(.)/g, function(match, group1) {
        return group1.toUpperCase();
    });
}

