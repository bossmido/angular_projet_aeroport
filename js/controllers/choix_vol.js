          //var app = angular.module("AppAeroport",[]);
           
            myApp.controller("choixVolController",['$scope','$sce','$templateRequest','$compile',function($scope,$sce,$templateRequest,$compile){
            var chaine_libelle = "choix_vol";
            $scope.title          = chaine_libelle;
            $scope.header         = chaine_libelle;
            $scope.tpl            = {};
            $scope.tpl.contentUrl = '/js/controllers/choix_vol.html';
            // select annoncé 
            
            $scope.ville          = villes;
            $scope.villes         = villes;
            $scope.nations        = nations;
            $scope.classes        = classes;
            
            //choix de la ville cité blabla pour cas d'exclusion à migrer en service.
            
            $scope.v1             = villes;
            $scope.v2             = villes;
            $scope.v1_selected    = "Tirana";
            $scope.v2_selected    = "Berlin";
            
            $scope.suite_retour2  = "";
            $scope.suite_retour   = "";
            
            compile_template($scope,$sce,$templateRequest,$compile,'js/controllers/aller_retour.html',"#aller_retour");
            compile_template($scope,$sce,$templateRequest,$compile,'js/controllers/choix_vol.html',"#choix_vol_component");
    
            $scope.init = function () {
                //window.$('.choix_date').datepicker({ dateFormat: 'yyy-dd-mm' });
                window.$('.choix_date').datepicker();                
            };
            $scope.init();

            $scope.$on("$destroy", function(){
                angular.module("AppAeroport",[]).value("depart",$scope.v1_selected);
                angular.module("AppAeroport",[]).value("arrivee",$scope.v2_selected);
            });
           
            
            }]);
