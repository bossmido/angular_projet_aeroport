window.myApp.factory('dependanceService',[ function($http) {

    var $http = angular.injector(["ng"]).get("$http");
    
    charger_dependance($http,"js","data","users-100.js");
    charger_dependance($http,"js","data","donnee_extra.txt");

    var resolveController = function(path) {
        var $injector = window.angular.injector(['ng']);
        var $q =$injector.get("$q");
        var deferred = $q.defer();
        var script = document.createElement('script');
        script.src = CONFIG.controller+""+path;
        
        script.onload = function() {
            $scope.$apply(deferred.resolve());
        };
        
        document.body.appendChild(script);
        return deferred.promise;
    };


    return true;

}]);
