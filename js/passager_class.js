function Voyage(a,b){}

Voyage.prototype.getdepart  = function (){
    return  this.depart;
}

Voyage.prototype.getarrivee = function (){
    return this.arrivee;
}

Voyage.prototype.setdepart = function (a){
    this.depart=a;
}
Voyage.prototype.setarrivee = function (b){
    this.arrivee=b;
}

Voyage.prototype.json = function (){
    return "{ depart :  "+this.depart+" , arrivee : "+this.arrivee+" }";
}


Voyage.prototype.construct = function(a,b){
    this.depart=a;
    this.arrivee=b;
    return this;
};


function Passager(){
    //{"id":3,"genre":"Male","prenom":"Gerald","nom":"Wells","email":"gwells2@dion.ne.jp","date_naissance":"2010/12/04","nationalite":"Poland","classe":"Premium","ville":{"depart":"Chisinau","arrivee":"Tirana"},"vol":"AC6355"},

    this.id=-1;
    this.genre="Male";
    this.prenom="...";
    this.nom="...";
    this.email="...@....com";   
    this.date_naissance="00/00/0000";
    this.nationalite="nationalite";
    this.ville=new Voyage("a","b");
    this.classe="Prenium";
    this.vol="AC6355";
    this.controle="<div></div>";
if(arguments.length>9){
    //console.log("%O",arguments);
    this.id=arguments[0];
    this.genre=arguments[1];
    this.prenom=arguments[2];
    this.nom=arguments[3];
    this.email=arguments[4];  


        this.date_naissance = arguments[5].split("/")[2]+"/"+arguments[5].split("/")[1]+"/"+arguments[5].split("/")[0];
        //this.date_naissance = arguments[5].split("-")[2]+"/"+arguments[5].split("-")[1]+"/"+arguments[5].split("-")[0];

    this.nationalite=arguments[6];

    this.classe=arguments[7];
    this.ville=arguments[8];

    this.vol=arguments[9];
    this.controle="";

}
    
    return this;

};


Passager.prototype.getArray=function(){
    return {
    'id':                this.id,
    'genre':             this.genre,
    'prenom':            this.prenom,
    'nom':               this.nom,
    'email':             this.email,   
    'date_naissance':    this.date_naissance,
    'nationalite':       this.nationalite,
    'ville':             this.ville,
    'classe':            this.classe,
    'vol':               this.vol,
    'controle' :         this.controle
    }
}


Passager.prototype.construct = function(){
        //{"id":3,"genre":"Male","prenom":"Gerald","nom":"Wells","email":"gwells2@dion.ne.jp","date_naissance":"2010/12/04","nationalite":"Poland","classe":"Premium","ville":{"depart":"Chisinau","arrivee":"Tirana"},"vol":"AC6355"},

    this.id=-1;
    this.genre="Male";
    this.prenom="...";
    this.nom="...";
    this.email="...@....com";   
    this.date_naissance="0000-00-00";
    this.nationalite="nationalite";
    this.ville=new Voyage("a","b");
    this.classe="Prenium";
    this.vol="AC6355";
    this.controle="<div></div>";
if(arguments.length>0){
    this.id=arguments[0].id;
    this.genre=arguments[0].genre;
    this.prenom=arguments[0].prenom;
    this.nom=arguments[0].nom;
    this.email=arguments[0].email;   
    this.date_naissance=arguments[0].date_naissance;
    this.nationalite=arguments[0].nationalite;
    this.ville=new Voyage("a","b");
    this.classe=arguments[0].classe;
    this.vol=arguments[0].vol;
    this.controle="<div controle ></div>";

}
    
    return this;


}
///////////////////////////////////////////////////


function PassagerCollection(){
    //{"id":3,"genre":"Male","prenom":"Gerald","nom":"Wells","email":"gwells2@dion.ne.jp","date_naissance":"2010/12/04","nationalite":"Poland","classe":"Premium","ville":{"depart":"Chisinau","arrivee":"Tirana"},"vol":"AC6355"},
    //this.collection=[];
   // this.choix_passager= "";

//    return this;

};



PassagerCollection.prototype.chargerPassager = function(){
    var data_local=[];
    this.collection=[];
    
    if(arguments.length > 1){//passage pas global de global user_data
        data_local=arguments[0];
    }else{
        data_local=window.data;//global du coup window
    }

    //console.log("%O",data_local);

    for(var arr_line in data_local){
//     console.log("%O",data_local[arr_line]);
        var voyage_json = (data_local[arr_line]['ville']);

        this.collection[data_local[arr_line]['id']] = 
        new Passager(
            data_local[arr_line]['id'],
            data_local[arr_line]['genre'],
            data_local[arr_line]['prenom'],
            data_local[arr_line]['nom'],
            data_local[arr_line]['email'],
            data_local[arr_line]['date_naissance'],
            data_local[arr_line]['nationalite'],
            data_local[arr_line]['classe'],
            new Voyage(voyage_json['depart'],voyage_json['arrivee']),
            data_local[arr_line]['vol']
        );

    }

}


PassagerCollection.prototype.filtre = function(champs,nom){
    var data_local=[];
   // this.collection=[];
    
    for(var arr_line in data_local){
        var regexp = new Regexp("(.*)"+nom+"(.*)",'i');
        if(data_local[arr_line][champs].match(regexp)){
        var voyage_json = (data_local[arr_line]['ville']);

        this.collection[data_local[arr_line]['id']] = 
        new Passager(
            data_local[arr_line]['id'],
            data_local[arr_line]['genre'],
            data_local[arr_line]['prenom'],
            data_local[arr_line]['nom'],
            data_local[arr_line]['email'],
            data_local[arr_line]['date_naissance'],
            data_local[arr_line]['nationalite'],
            data_local[arr_line]['classe'],
            new Voyage(voyage_json['depart'],voyage_json['arrivee']),
            data_local[arr_line]['vol']
        );

    }
}

}



PassagerCollection.prototype.choisirPassager = function (id){
    this.choix_passager = id;
    return this.collection[id];
}


PassagerCollection.prototype.majpassager = function (id,Passager_param){
    this.collection[Passager_param['id']] =Passager_param; 
    return Passager_param;
}



PassagerCollection.prototype._recalculerIds = function (){

var inc_deplacement=0;
    for(var arr_line in this.collection){
      //  var voyage_json = JSON.parse(data_local[arr_line]['ville']);

        if(this.collection[this.collection[arr_line]] == null){
            this.collection.splice(this.collection[arr_line],1);
            for(var i=inc_deplacement;i<(collection.length-inc_deplacement);i++){
                this.collection[i]['id'] =(parseInt(this.collection[i]['id'])-1)+""; 
            }
        }else{

        }
        

    }

    inc_deplacement++;
}

PassagerCollection.prototype.supprimerpassager = function (id,Passager_param){

    if(typeof Passager_param == "object" ){
        this.collection[Passager_param['id']] =null; 
    }else{
                this.collection[Passager_param] =null; 
    }
    this._recalculerIds();
    return Passager_param;
}


PassagerCollection.prototype.ajouter = function (Passager_param){
    var id_davant =this.collection.slice(-1).pop()['id'] ;
    this.collection[(ParseInt(id_davant)+1)+""]=Passager_param;
    this.collection["id"]=(ParseInt(id_davant)+1)+"";
}


/*
PassagerCollection.prototype.ajouter = function (Passager_param){
    var id_davant =this.collection.slice(-1).pop()['id'] ;
    this.collection[(ParseInt(id_davant)+1)+""]=Passager_param;
    this.collection["id"]=(ParseInt(id_davant)+1)+"";
}
*/

PassagerCollection.prototype.getListeNationalite= function (){
    var inc_deplacement=0;
    var output_arr=[];
    var nationalite_unique=[];
    for(var arr_line in this.collection){
        output_arr.push(this.collection[arr_line]['nationalite']);
    }

/*    $.each(inc_deplacement, function(i, el){
        if($.inArray(el, nationalite_unique) === -1) nationalite_unique.push(el);
    });
  console.log("%O",nationalite_unique);
*/
  return output_arr;
}


PassagerCollection.prototype.getArray= function (){
 var output=[];
    for(var arr_line in this.collection){
        output.push(this.collection[arr_line].getArray());
    }

    //console.log("%O",output);
return output;
}

PassagerCollection.prototype.getCollection= function (){
//    console.log("%O",this.collection);
    return this.collection;
};


PassagerCollection.prototype.construct= function (){
    //{"id":3,"genre":"Male","prenom":"Gerald","nom":"Wells","email":"gwells2@dion.ne.jp","date_naissance":"2010/12/04","nationalite":"Poland","classe":"Premium","ville":{"depart":"Chisinau","arrivee":"Tirana"},"vol":"AC6355"},
    this.collection=[];
    this.choix_passager= "";
    choix_passager=null;

  //  return this.prototype;

};

