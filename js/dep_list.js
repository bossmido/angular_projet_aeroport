define("dep_list",{
    "domready"         : "domReady",
    //
    "defer_js"         : "vendor/DeferJS",
    "defer_jquery"     : "vendor/jquery.defer",
    "jquery"           : "vendor/jquery",
    "jquery-ui"        : "vendor/jquery-ui/jquery-ui",
    //
    "popper"           : "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min",
    "bootstrap"        : "vendor/bootstrap/js/dist/index",
    "tpls"             : "vendor/bootstrap/js/dist/ui-bootstrap-tpls-2.5.0.min",
    //
    "angular"          : "vendor/angularjs/angular",
    "angular-sanitize" : "vendor/angularjs/angular-sanitize",
    "angular_route_1"  : "vendor/angularjs/angular-route_1",
    "angular-route"    : "vendor/angularjs/angular-route",
    //
    "fonctions"        : "fonctions",
    "style_color_random" :"style_color_random",
    "init_module"      : "init_module",
    "routes"           : "routes",
    "main"             : "app_aeroport",
    "filtre_recherche_client" : "filters/filtre_recherche_client",
    "init_jquery_ui"   : "init_jquery_ui"
    
});